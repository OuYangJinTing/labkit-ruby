# frozen_string_literal: true

require "active_support/json"

require "./spec/support/grpc_service"

describe Labkit::Logging::GRPC::ServerInterceptor do
  describe "running on RpcServer" do
    subject { described_class.new(output, default_tags) }

    let(:output) { StringIO.new }
    let(:default_tags) { {} }
    let(:result) { JSON.parse(output.string) }

    def mock_server
      server = LabkitTest::TestService::MockServer.new
      yield server.start(server_interceptors: [Labkit::Correlation::GRPC::ServerInterceptor.new, subject])
    ensure
      server.stop
    end

    describe "#req_res_method" do
      it "creates a log message" do
        mock_server do |client|
          client.req_res_method(LabkitTest::Msg.new)
        end

        expect(result).to include("grpc.code" => "OK")
        expect(result).not_to include("exception")
      end
    end

    describe "#server_stream_method" do
      it "creates a log message" do
        mock_server do |client|
          enumerator = client.server_stream_method(LabkitTest::Msg.new)
          enumerator.each { } # Consume the stream
        end

        expect(result).to include("grpc.code" => "OK")
        expect(result).not_to include("exception")
      end
    end

    describe "#client_stream_method" do
      it "creates a log message" do
        mock_server do |client|
          client.client_stream_method([LabkitTest::Msg.new])
        end

        expect(result).to include("grpc.code" => "OK")
        expect(result).not_to include("exception")
      end
    end

    describe "#bidi_stream_method" do
      it "creates a log message" do
        mock_server do |client|
          enumerator = client.bidi_stream_method([LabkitTest::Msg.new])
          enumerator.each { } # Consume the stream
        end

        expect(result).to include("grpc.code" => "OK")
        expect(result).not_to include("exception")
      end
    end

    describe "log fields" do
      it "sets the expected fields" do
        mock_server do |client|
          client.req_res_method(LabkitTest::Msg.new)
        end

        expect(result).to include(
          "grpc.code" => "OK",
          "grpc.method" => "ReqResMethod",
          "grpc.service" => "labkit_test.TestService",
          "pid" => Process.pid,
        )

        expect(result["correlation_id"]).to match(/[0-9a-f]{10}/)
        expect(result["time"]).to match(/^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{3}Z$/)
        expect(result["grpc.start_time"]).to match(/^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}Z$/)

        time_ms = result["grpc.time_ms"]
        expect(time_ms).to be_a Float
        expect(time_ms.to_s).to match(/^[0-9]\.[0-9]{1,3}$/)
      end

      describe "when default tags are provided" do
        let(:default_tags) { { "foo" => 123, "hello" => "world" } }

        it "includes the default tags in log messages" do
          mock_server do |client|
            client.req_res_method(LabkitTest::Msg.new)
          end

          expect(result).to include(default_tags)
        end
      end
    end

    describe "error codes" do
      it "logs GRPC exceptions with the corresponding names" do
        mock_server do |client|
          expect do
            client.req_res_method(LabkitTest::Msg.new(error_code: ::GRPC::Core::StatusCodes::DATA_LOSS))
          end.to raise_error(::GRPC::DataLoss)
        end

        expect(result).to include("grpc.code" => "DataLoss")
        expect(result).to include("exception" => "15:test exception")
        expect(result["exception_backtrace"]).to be_a(Array)
      end

      it "logs other exceptions as Unknown" do
        mock_server do |client|
          expect do
            client.req_res_method(LabkitTest::Msg.new(error_code: -1))
          end.to raise_error(::GRPC::Unknown)
        end

        expect(result).to include("grpc.code" => "Unknown")
        expect(result).to include("exception" => "-1")
        expect(result["exception_backtrace"]).to be_a(Array)
      end
    end
  end
end
