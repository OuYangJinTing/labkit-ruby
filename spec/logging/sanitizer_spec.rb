# frozen_string_literal: true

require "spec_helper"

describe Labkit::Logging::Sanitizer do
  using RSpec::Parameterized::TableSyntax

  describe ".sanitize_field" do
    def sanitizable_text(url)
      # We want to try with multi-line content because is how error messages are formatted
      %(
         remote: Not Found
         url: "#{url}"
         fatal: repository '#{url}' not found
      )
    end

    where(:input, :output) do
      "http://user:pass@test.com/root/repoC.git/" | "http://*****:*****@test.com/root/repoC.git/"
      "https://user:pass@test.com/root/repoA.git/" | "https://*****:*****@test.com/root/repoA.git/"
      "ssh://user@host.test/path/to/repo.git" | "ssh://*****@host.test/path/to/repo.git"
      "ssh://user@192.168.0.2/path/to/repo.git" | "ssh://*****@192.168.0.2/path/to/repo.git"

      # git protocol does not support authentication but clean any details anyway
      "git://user:pass@host.test/path/to/repo.git" | "git://*****:*****@host.test/path/to/repo.git"
      "git://host.test/path/to/repo.git" | "git://host.test/path/to/repo.git"
      "git://username:password@[fe80::74e6:b5f3:fe92:830e]/project.git" | "git://*****:*****@[fe80::74e6:b5f3:fe92:830e]/project.git"

      # SCP-style URLs are sanitized correctly
      "user@server:project.git" | "*****@server:project.git"
      "user:pass@server:project.git" | "*****:*****@server:project.git"
      "user@server.fqdn.com:project.git" | "*****@server.fqdn.com:project.git"
      "user:pass@server.fqdn.com:project.git" | "*****:*****@server.fqdn.com:project.git"
      "username@[fe80::74e6:b5f3:fe92:830e]:project.git" | "*****@[fe80::74e6:b5f3:fe92:830e]:project.git"
      "username@192.168.0.2:project.git" | "*****@192.168.0.2:project.git"

      # return an empty string for invalid URLs
      "ssh://" | ""
    end

    with_them do
      it { expect(described_class.sanitize_field(sanitizable_text(input))).to eq(sanitizable_text(output)) }
    end
  end

  describe ".mask_url" do
    where(:input, :output) do
      "http://user:pass@test.com/root/repoC.git/" | "http://*****:*****@test.com/root/repoC.git/"
      "https://user:pass@test.com/root/repoA.git/" | "https://*****:*****@test.com/root/repoA.git/"
      "ssh://user@host.test/path/to/repo.git" | "ssh://*****@host.test/path/to/repo.git"

      # git protocol does not support authentication but clean any details anyway
      "git://user:pass@host.test/path/to/repo.git" | "git://*****:*****@host.test/path/to/repo.git"
      "git://host.test/path/to/repo.git" | "git://host.test/path/to/repo.git"

      # return an empty string for invalid URLs
      "ssh://" | ""
    end

    with_them do
      it { expect(described_class.mask_url(input)).to eq(output) }
    end
  end

  describe ".mask_scp_url" do
    where(:input, :output) do
      "user@server:project.git" | "*****@server:project.git"
      "user:pass@server:project.git" | "*****:*****@server:project.git"
      "user:pass@host_name:project.git" | "*****:*****@host_name:project.git"
      "user:pass@server.fqdn.com:project.git" | "*****:*****@server.fqdn.com:project.git"
      "user:pass@server.fqdn.com:group/project.git" | "*****:*****@server.fqdn.com:group/project.git"
      ":pass@server.fqdn.com:project.git" | ""
      "username::::::::::pass@server.fqdn.com:project.git" | "*****:*****@server.fqdn.com:project.git"
      "username@[fe80::74e6:b5f3:fe92:830e]:project.git" | "*****@[fe80::74e6:b5f3:fe92:830e]:project.git"
    end

    with_them do
      it { expect(described_class.mask_scp_url(input)).to eq(output) }
    end
  end

  describe ".sanitize_sql" do
    where(:input, :output) do
      "invalid sql" | ""
      "select 42" | "select $1"
      %q[SELECT "routes".* FROM "routes" WHERE "routes"."source_type" = 'Namespace' AND "routes"."source_id" IN (1, 22, 23)] | %q[SELECT "routes".* FROM "routes" WHERE "routes"."source_type" = $1 AND "routes"."source_id" IN ($2, $3, $4)]
    end

    with_them do
      it { expect(described_class.sanitize_sql(input)).to eq(output) }
    end
  end

  describe ".sql_fingerprint" do
    where(:input, :output) do
      "invalid sql" | ""
      "select 42" | "50fde20626009aba"
      %q[SELECT "routes".* FROM "routes" WHERE "routes"."source_type" = 'Namespace' AND "routes"."source_id" IN (1, 22, 23)] | "139b5a9eb28b075a"
    end

    with_them do
      it { expect(described_class.sql_fingerprint(input)).to eq(output) }
    end
  end
end
